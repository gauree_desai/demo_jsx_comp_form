import React, { Component } from 'react'

export default class Form extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            username:'',
            comments:'',
            topic:'react',
            
        }
    
    }
    
    handleUsernamechange = event =>{
        this.setState({
            username:event.target.value
        })
    }

    handleCommentschange = event =>{
        this.setState({
            comments:event.target.value
        })
    }


    handleTopicchange = event =>{
        this.setState({
            topic:event.target.value
        })
    }



    handleSubmit= event=>{
        alert(`${this.state.comments} ${this.state.username} ${this.state.topic}`)
        event.preventDefault()
    }
    render() {
        return (
            <div>
              <form onSubmit={this.handleSubmit}>
              <fieldset>
                  <div>
                      <fieldset>
                        <legend>Username:</legend>
                        <input 
                            type="text" 
                            value={this.state.username} 
                            onChange={this.handleUsernamechange}
                        />
                   </fieldset>
                  </div>
                  <div>
                      <fieldset>
                      <legend>Comments:</legend>
                      <textarea 
                          value={this.state.comments}
                          onChange={this.handleCommentschange}
                     />
                     </fieldset>
                  </div>
                  <div>
                      <fieldset>
                      <legend>Topic:</legend>
                      <select value={this.state.topic} onChange={this.handleTopicchange}>
                          <option value="react">React</option>
                          <option value="angular">Angular</option>
                          <option value="vue">Vue</option>
                      </select>
                      </fieldset>
                  </div>
                  <br/>
                 <button type="submit" >Submit</button> 
                 </fieldset> 
              </form>
             
            </div>
        )
    }
}
