import Address from "./Address";

function Footer (props){
    
    return (
     <div>
       <h3>Copyright {props.year}</h3>
       <Address></Address>
     </div>   
    
    )
}

export default Footer;