import React, { Component } from 'react'

export default class Address extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             country:"India",
             pin:123456
        }
    }
    
    render() {
        return (
            <div>
                <hr/>
               <h5>{this.state.country}</h5>
               <h6>{this.state.pin}</h6> 
            </div>
        )
    }
}
