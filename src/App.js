import logo from './logo.svg';
import './App.css';
import Header from './Components/Header';
import Footer from './Components/Footer';
import Main from './Components/Main'

function App() {
  return (
    <div>
      <Header></Header>
      <Main topic="React"></Main>
      <Footer year="2021" name="Edureka" age="90"></Footer>
    </div>
  );
}

export default App;
